# Focus Management

## What is Deep Work?

- Deep Work is the ability to focus on one task at a time without any distractions so that our cognitive capabilities are pushed to their limits.


## Summary of Deep Work.

- Intense periods of focus in isolated fields of work cause myelin to develop in relevant areas of the brain which makes neural transmissions faster and clearer.
- Deep work allows us to connect ideas and uncover and create solutions rapidly
- The ability to do Deep Work is increasingly valuable and increasingly rare.
- We can implement the following three strategies for deep work :
    1. We can schedule our distractions.
    2. Setting up a time for deep work every day and sticking to it. Consistently put in at least four hours of deep work every day.
    3. Following a daily shutdown ritual. We can note down the day's pending tasks and transfer them to the next day. Prioritizing sleep is also vital for deep work.

## How can you implement the principles in your day-to-day life?

- I can turn off all the notifications and sign out of all the social media apps while I’m working.
- I could allocate some time for distractions so it doesn’t seem like a herculean task to concentrate on all day.
- I can follow a schedule for deep work so I don’t have to make a decision to do deep work every time.

## Dangers of Social Media

- Usage of social media is linked to increases in anxiety.
- Hinders our ability to focus.
- Exposure to only the best parts of the lives of people on the internet makes us rethink our own lives and makes us feel inadequate causing depression and other issues.
- It never allows us to completely shut down and restore ourselves.

