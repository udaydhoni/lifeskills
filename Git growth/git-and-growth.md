# Grit and Growth Mindset

##  1. Grit

### Paraphrase (summarize) the video in a few lines. Use your own words.

Grit is tenacity and resilience in a challenging environment. Irrespective of individual skill set, IQ, and other factors, if anyone wants to learn he needs to be keen to learn and should have strong willpower.

### What are your key takeaways from the video to take action on?

I understood that success is not a function of natural talent but our grit. The key to success is not giving up in the face of failure.

## 2. Introduction to Growth Mindset

### Paraphrase (summarize) the video in a few lines in your own words.

There are two fundamentally different viewpoints about skill and learning.

- Fixed mindset

1. People having this mindset believe that their skills and intelligence are set.


- Growth mindset

2. People having this mindset believe that skills and intelligence can be learned and developed.


### What are your key takeaways from the video to take action on?

It is important to understand that most skills can be learned no matter how hard they may seem the only thing we need to have is grit.

## 3. Understanding Internal Locus of Control.

### What is the Internal Locus of Control? What is the key point in the video?

If we believe that our success is because of some external factors like born intelligence and all we're not likely to work hard on our goals and if we believe that our success is because of our hard work then we're more likely to work harder and achieve results.

### Paraphrase (summarize) the video in a few lines in your own words.

We grow not because of our born talents but because of our willingness to work hard and put in the work.

### What are your key takeaways from the video to take action on?

Don't take our abilities for granted to reach the top and to stay there we need to put in hard work.

## 4. Mindset - A MountBlue Warrior Reference Manual

### What are one or more points that you want to take action on from the manual? (Maximum 3)

1. I will stay relaxed and focused no matter what happens.
2. I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.
3. I will take ownership of the projects assigned to me. Its execution, delivery, and functionality are my sole responsibility.



