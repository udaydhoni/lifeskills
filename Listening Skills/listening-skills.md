# Listening and Active Communication

## 1. Active Listening

What are the steps/strategies to do Active Listening?

1. Avoid getting distracted by your own thoughts. Focus on the speaker and the topic.
2. Avoid interrupting the speaker.
3. Use door openers that show that you're interested.
4. Try to paraphrase what the speaker said to build rapport.
5. Don't think of the reply you might have to give.
6. Have body language that shows you're interested.

## 2. Reflective Listening

According to Fisher's model, what are the key points of Reflective Listening?

1. Reflective listening is a process  of genuinely understanding the speaker by focusing on his words and body language and reflecting upon the understanding you have.
2. The listener must be empathetic.
3. Take notes in key meetings.
4. Share the notes to make sure you've understood.

## 3. Reflection

What are the obstacles in your listening process?

1. In a one-to-one conversation, I tend to focus more on what my reply should be than on really understanding the other person.
2. I lose focus if the conversation is too long and only the other person is speaking.

What can you do to improve your listening?

1. I need to be in the present moment and just focus on what the other person is saying.

## 4. Types of Communication

When do you switch to a Passive communication style in your day-to-day life?
1. When I don't know the person that I'm talking with.

When do you switch to Aggressive communication styles in your day-to-day life?
1. When I get angry or when I am fed up with certain kinds of behavior or people.

When do you switch to a Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day-to-day life?
1. When the person I expect to understand my feelings fails to understand them.

How can you make your communication assertive? You can analyze the videos and then think about what steps you can apply in your own life.
1. I need to understand that my needs and feelings are important.
2.  I don't have to please everybody and even if I try I cannot do so.
3.  Being respectful of other people as well while communicating about my own needs.
