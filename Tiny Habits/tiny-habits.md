# Tiny Habits - BJ Fogg

## Q1. Your takeaways from the video

- Tiny tasks has the potential to bring positive outcomes in life.
- Tiny tasks require less willpower
- As they're less demanding easy to get habituated to them.

# Tiny Habits by BJ Fogg - Core Message

## Q2. Your takeaways from the video in as much detail as possible.

- The video explains how BJ gave the (B=MAP) behavior model, where he explained that behavior change is possible not only by motivation but can also be achieved by creating or breaking down a change to its most minute action and making that a habit. Now he explained why this method works in the following points

1. They’re fast to do, can fit into any schedule, and allows you to start now.
2. They’re safe, unlike big or risky actions.
3. If you make a mistake, it’s easy to try again.
They’re so tiny that you don’t need to rely on your limited willpower.
4. Yet, they add up over time to deliver dramatic changes.

## Q3. How can you use B = MAP to make making new habits easier?

- Behavior is the result of motivation, ability, and prompts taken at the same time. This is a simple formula that can lead to incredible results. In other words, a behavior is the result of Motivation, or your desire to execute the behavior.Ability, i.e. your capacity to execute the behavior.Prompt, or your cue to execute the behavior.This is true for building a habit and breaking an old habit in equal measure.

## Q4.Why it is important to "Shine" or Celebrate after each successful completion of a habit?

- When you celebrate, you create a positive feeling inside yourself on demand. This good feeling wires the new habit into your brain.

# 1% Better Every Day Video

## Q5. Your takeaways from the video.

- A little improvement every day lead to achieving bigger goals in the long term process Long-term changes can not be achieved in one day it is a time taking and step-by-step process

# Book Summary of Atomic Habits

## Q6. Write about the book's perspective on habit formation from the lens of Identity, processes, outcomes?

 - The book suggested many ways of adapting any habit and making that habit your style of living,With outcome-based habits, the focus is on what you want to achieve. These types of habits are the weekly, daily, minute-by-minute habits breaking bad behaviors and adopting good ones in four steps, showing you how small, incremental, everyday routines compound into massive, positive change over time

 ## Q7. Write about the book's perspective on how to make a good habit easier?

 - Building good habits in 7 steps When building habits, you will often be replacing old ones. Here are seven ways to replace bad habits with healthier ones:

 1. Eliminate triggers
 2. Make a negative habit difficult
 3. Uncover the root
 4. Swap a bad habit for a better habit
 5. Build motivation

 ##  Q8. Write about the book's perspective on making a bad habit more difficult?

- we need to replace unhealthy behaviour with healthy behaviour. need to prepare mentally. Reward your self for small steps.

# Reflection:

## Q9. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- I want to make my day satisfying. It will be done when I am responsive. I want to become one good software engineer I will work smarter way to achieve my goals every day and freshly start my day with some practice and change myself it is my goal. This makes me more satisfying.

## Q10. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

- One of my bad habits of mine is negative thinking. I want to eleminate this habit of mine and change by trying to live in present.



