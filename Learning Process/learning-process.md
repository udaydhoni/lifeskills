# Learning Process

## Feynman Technique Summary

It is a technique of learning that physicist Richard Feynman used. There are four steps in this technique:

- Study.

Study a particular concept.

- Teach.

After feeling confident about that topic try to teach it to someone else.

- Fill the Gaps.

If you uncover some gaps in your knowledge revisit the source material and also take the help of others if needed.

- Simplify.

Try to simplify the concept so that even a kid could understand.

### How could we implement it?

- Write some code or learn a concept.

- Explain it to a friend.

- If there are any gaps in our understanding we need to revise the topics again.

- Simplify it to that level where a layman could understand.


## Learning "How to Learn" - Barbara Oakley

### Summary

The thinking processes can be fundamentally classified into focussed and diffused modes.

- Focussed mode.

Keeping our entire attention on which we're working.

- Diffused mode.

Relaxing so that new patterns of thought could emerge.

When we're learning our brain goes back and forth between these two modes of the brain. So, for optimum learning outcomes, we need to 
take breaks to get into the diffused mode of thinking after focusing intensely.


#### Procrastination

Postponing a set of tasks is known as procrastination. This behavior kicks in as a response to avoid the pain of doing something that 
we would not like to do.

The best way to overcome this is by getting started with the work at hand. Using a Pomodoro and setting aside a set amount of time for deep focus and then relaxing for a set amount of time. This technique helps in effectively working with two modes of brain.

Some of our seemingly worst traits are actually our best traits.

- Bad Memory: Research shows that people who have bad memory are actually quite creative.

- Slow thinking: Slow thinkers will have a solid understanding of whatever they study.

### Advice

- Exercise: Helps in building new neural pathways.

- Tests: Tests are the best way to solidify our understanding.

- Repetition: Repeating the tests or drills.

- Recall: Recalling what you've studied.

Understanding alone is not enough for mastery, repetition and practice are also crucial.

### Steps that I could implement.

1. Taking care of physical health.

2. Constant practice and repetition.

3. Testing my knowledge through coding exercises.

4. Using Pomodoro for productivity.


## Learn anything in 20 hours

### Key takeaways

- 10,000 hours rule is to become an ultra performer in a field.

- 20 hours of deliberate practice is all that is needed to get from knowing nothing to knowing up to a point where you can self-correct.

- Before starting our practise we need to deconstruct the skill and focus on only those areas where we want to build ourselves.

- The more we break down a skill and focus on only important things the better we would become.

- Remove barriers to practice.

- You will feel stupid while starting something and that's okay.


### Steps that I could take

- Start by really understanding what is important and necessary and focusing on only that initially.

- Removing distractions and thereby not making our practise a willpower challenge.

- Relying on a few resources but not too many till we reach  a point where we could self-correct.





