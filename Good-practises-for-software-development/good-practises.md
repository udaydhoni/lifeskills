# Good Practises for Software Development

## What is your one major takeaway from each one of the 6 sections. So 6 points in total.

1. I will make sure to ask questions and seek clarity in the meeting itself. I will try to maintain notes for the requirements.

2. I will use group chat/ channels to communicate whenever it is possible.

3. I will try to make my question as comprehensible as possible so that the other person finds it easy to answer.

4. I will try to build a good rapport with team members so that it becomes easy to communicate with them.

5. I will not bombard my team members with multiple questions but instead, I'll write all my questions into one single message.

6. I will try to avoid distractive apps while I'm working.

## Which area do you think you need to improve on? What are your ideas to make progress in that area?

1. I think I need to improve in the way I ask for help, I will try to explain the problem as clearly as possible and the solutions I've tried.

2. I will also communicate with the relevant team members when I got stuck anywhere.
