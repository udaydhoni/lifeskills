# Prevention of Sexual Harassment

## What kinds of behaviour cause sexual harassment?

Sexual harassment could be in any of the following three forms.

- Verbal harassment

Comments about clothing, body, sexual or gender-based jokes, requesting sexual favours or repeatedly asking a person out, indirect signals, threats, using foul and obscene language.

- Visual harassment

Obscene posters, drawings, screensavers, cartoons, emails or texts of sexual nature.

- Physical harassment

Sexual assault, Impeding or blocking movement, Inappropriate touching etc.

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?

We must report it when we see it or experience it to higher authorities. If there is no proper response then we need to seek legal help.

## Explain different scenarios enacted by actors.

1. Artistic Freedom

A male coworker puts up a photo which is sexual on the bulletin board and asks his female coworker about it.

2. You gotta keep trying

A female coworker keeps on asking her colleague for a date even when he refuses the proposal.

3. The Joke's on you

A female coworker sends emails which she thinks are funny but it is offensive to the other actor involved.

4. The Legend

Where a male coworker gives bare hugs to everyone but the other person feels that to be inappropriate.

5. ODD MAN out

The male boss shows special attention to  a young female worker which is perceived as a sexual quid pro quo by the other lady.

6. The Contract

The female actor hints at a sexual favour in return for an agreement.

7. Twisted Words

The male coworker comments about the female coworker's work.

8. Equal Appreciation

Both the male and female coworkers comment on the actor's outfit but the actor gets offended by the female coworker's comment.

9. Everyone loves a compliment

The male actor gives the usual compliments but the female actor is not comfortable with it.

10. Testing the waters

The male coworker tests the female coworker for returning sexual favours.

## How to handle cases of harassment?

### If you are the victim 

- Complaint it to the higher authorities.

- If the issue is not sorted then take legal help.

- It is advisable to have evidence in your favour.

- Take the help of other coworkers.


## How to behave appropriately?

- Do not cross personal boundaries.

- If you would not like it to happen to your family members then it is not ideal behaviour.

- Don't say, do, or send anything sexually suggestive.

- You need to refrain from using obscene language in the workplace.






