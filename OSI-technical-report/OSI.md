# OSI Model - Open Systems Interconnection Model 

## What is an OSI model ?



It is a conceptual framework which describes seven layers that computers on a network use to communicate over a network. It was the first standard model for network communications which was adopted by ISO (International Organization for Standardization) in 1984. This framework makes troubleshooting easier.

The seven layers of OSI in a "top down" manner are as follows:

- Application Layer
- Presentation Layer
- Session Layer
- Transport Layer
- Network Layer
- Data Link Layer
- Physical Layer

![alt OSI image](OSI-technical-report/images-for-report/osi-rm-4.png)

## Layer 7 - Application

It is a layer that is closest to the end user. It receives information from the end user and displays the incoming data to the user. This happens through a set of protocols. The applications that the user interacts with does not exist in this layer but this layer facilitates communication through lower layers in order to establish communication with the applications at the other end. Browsers are good examples of communications that rely on this layer. HTTPS , FTP, POP, DNS are some examples of the protocols in this layer.

![alt application layer image](OSI-technical-report/images-for-report/application.png)

## Layer 6 - Presentation

This layer prepares how the data should be presented to the application layer. It defines how two devices should encode, encrypt, compress data so it is received correctly on the other end. This is the reason it is also called as **Translational layer**.

![alt Presentation Image](OSI-technical-report/images-for-report/presentation-layer.jpg)


## Layer 5 - Session

This layer creates communication channels called sessions between devices. It is responsible for establishing connection, maintenance of sessions, which is ensuring that they remain open and functional while data is being transferred and termination. This layer is also responsible for authentication and also for ensuring security. This layer also sets checkpoints during a data transfer so the data transfer can resume from the last checkpoint if interrupted.

![alt Session Layer image](OSI-technical-report/images-for-report/session.png)

## Layer 4 - Transport

This layer takes data transferred in the session layer and breaks into segments on the transmitting end. It is responsible for reassembling the segments on the recievers end, turning it back to data that can be used by session layer. It also controls the flow of data transfer matching the rate of data transfer to the speed of receiving device. It also checks whether the data received is correct, if it is incorrect it requests the data again.

![alt Transport layer image](OSI-technical-report/images-for-report/transport.png)


## Layer 3 - Network

The two main functions of this layer are as follows:
- Breaking up segments into network packets and reassembling them on the receiving end.
- The other function is to route these packets by discovering best path across a physical network. This is done typically using network addresses such as IP.

![alt Network layer image](OSI-technical-report/images-for-report/network.png)

## Layer 2 - Data Link

This layer is responsible for the node to node delivery of the message. The main function of this layer is to check that the data transfer is error free. This layer breaksup the network packets into frames and then sends them from source to destination. It is composed of two parts - (LLC) Logical Link Control which identifies network protocols, performs error checking and synchronizes frames, and Media Access Control (MAC) which uses MAC addresses to connect devices and define permissions to transmit and receive data.

![alt Data Link Layer image](OSI-technical-report/images-for-report/datalink.png)

## Layer 1 - Physical 

This layer is responsible for actual physical connection between the devices. In this layer the information is in the form of bits. It is responsible for transmitting individual bits from one node to another When receiving data, this layer will get the signal received and convert it into 0s and 1s and send them to the Data Link layer, which will put the frame back together.

![alt physical layer image](OSI-technical-report/images-for-report/physical.jpg)

## Advantages of OSI

- It is easy to troubleshoot by identifying which layer is causing an issue and resolving it quickly.

- Since it is layered, changes in one layer does not effect the other if the interfaces between the layers are not changed.
- It acts as guidance tool to develop any network model.

## References

- [GeeksforGeeks](https://www.geeksforgeeks.org/layers-of-osi-model/)
- [TechTerms](https://www.youtube.com/watch?v=vv4y_uOneC0")
- [cloudfare](https://www.cloudflare.com/learning/ddos/glossary/open-systems-interconnection-model-osi/)
- [Python Life](https://www.youtube.com/watch?v=DxVxqVg21Cg)





