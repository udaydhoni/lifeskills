# Energy Management

## Manage Energy Not Time

### What are the activities you do that make you relax - Calm quadrant?
  -  Talking to people close to my heart, watching films or documentaries, and listening to songs.

### When do you find getting into the stress quadrant?
- When I overthink  the future outcomes. 

###  How do you understand if you are in the Excitement quadrant?
- If I'm too happy or if someone surprises me or shocks me.

### Paraphrase the Sleep is your Superpower video in detail.
- Not sleeping well is harmful to the physical and mental well-being of the body. 
- It causes various health issues from cardiovascular diseases to reproductive health issues.
- Maintaining regularity in going to sleep is also important. 
- We cannot pay for one night's loss of sleep the other day.
- We should go to bed only when we're sleepy so that our brain associates bed with sleep.

### What are some ideas that you can implement to sleep better?
- Going to bed on time.
- Not being awake in bed.
- Regular and simple exercise.

### Paraphrase the video - Brain Changing Benefits of Exercise.
 - Exercise has immediate effects on our brains.
 - Improve reaction times.
 - Exercise produces new brain cells.
 - Improves attention.
 - Delays prefrontal cortex deterioration.

### What are some steps you can take to exercise more?

- Doing simple exercises like situps or squats.
- Going for a walk.
- Taking stairs

